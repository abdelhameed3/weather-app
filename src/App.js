import React, { Component } from 'react';
import Weather from './component/Weather';
import Form from './component/Form';
import './style/css/weather-icons.css'

const API_KEY = "e36ed364400282e43250b6c4c0274d44";
class App extends Component {
  constructor(){
   super();
   this.state = {
    city: '',
    country: '',
    humidity: '',
    description: '',
    error: '',
    icon:'',
    celsius: undefined,
    temp_max: null,
    temp_min: null,
  }
  this.weatherIcon  = {
    Thunderstorm: "wi-thunderstorm",
      Drizzle: "wi-sleet",
      Rain: "wi-storm-showers",
      Snow: "wi-snow",
      Atmosphere: "wi-fog",
      Clear: "wi-day-sunny",
      Clouds: "wi-day-fog"
  }
 }
  
 calCelsius(temp) {
  let cell = Math.floor(temp - 273.15);
  return cell;
}
  getWeather = async (e) => {
    e.preventDefault();
    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;
    let api;
   if(city && country){
     api = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}`);
  
    const data = await api.json();
    
      this.setState({
        city: data.name,
        country: data.sys.country,
        humidity: data.main.humidity,
        description: data.weather[0].description,
        celsius: this.calCelsius(data.main.temp),
        temp_max: this.calCelsius(data.main.temp_max),
        temp_min: this.calCelsius(data.main.temp_min),
        error: ''

      })
      this.get_WeatherIcon(this.weatherIcon, data.weather[0].id);

    } else {
      this.setState({
        city: '',
        country: '',
        humidity: '',
        description: '',
        icon:'',
        celsius: undefined,
        temp_max: null,
        temp_min: null,
        error: 'Please Enter Data'
      })
    }
  }

    get_WeatherIcon = (icons, rangeId)  => {
      switch (true) {
        case rangeId >= 200 && rangeId < 232:
          this.setState({ icon: icons.Thunderstorm });
          break;
        case rangeId >= 300 && rangeId <= 321:
          this.setState({ icon: icons.Drizzle });
          break;
        case rangeId >= 500 && rangeId <= 521:
          this.setState({ icon: icons.Rain });
          break;
        case rangeId >= 600 && rangeId <= 622:
          this.setState({ icon: icons.Snow });
          break;
        case rangeId >= 701 && rangeId <= 781:
          this.setState({ icon: icons.Atmosphere });
          break;
        case rangeId === 800:
          this.setState({ icon: icons.Clear });
          break;
        case rangeId >= 801 && rangeId <= 804:
          this.setState({ icon: icons.Clouds });
          break;
        default:
          this.setState({ icon: icons.Clouds });
      }
    }
  render() {
    

    return (
      <div className="wrapper">
        <div className="App">
          <Form getWeather={this.getWeather} />
          <Weather propsData={this.state} />

        </div>
      </div>
    );
  }
}

export default App;
