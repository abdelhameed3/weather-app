import React, { Component } from 'react';
const Weather = (props) => {
    return (
        <div className="Weather">

            {/* {
                props.propsData.city && <p><span>City :</span> {props.propsData.city} </p>
            }
            {
                props.propsData.country && <p><span>Country :</span> {props.propsData.country} </p>
            }
            {
                props.propsData.humidity && <p><span>Humidity :</span> {props.propsData.humidity} </p>
            }
            {
                props.propsData.description && <p><span>Description :</span> {props.propsData.description} </p>
            }
            {
                props.propsData.error && <p><span>Error :</span> {props.propsData.error} </p>
            } */}

{
                props.propsData.error && <p><span>Error :</span> {props.propsData.error} </p>
            } 
            {props.propsData.city && <h1 className=" py-3">  {props.propsData.city} <span> , </span>  {props.propsData.country}  </h1>}
            {/* Get Icon */}

            {
                props.propsData.icon &&
                <h5 className="py-4">
                    <i className={`wi ${props.propsData.icon} display-1`} />
                </h5>

            }


            {/* Get Temp */}
            {props.propsData.celsius && <h1 className="py-2">{props.propsData.celsius}&deg;</h1>}


            {/* show max and min temp */}
            {maxminTemp(props.propsData.temp_min, props.propsData.temp_max)}

            {/* Weather description */}
            {
                props.propsData.description &&
                <h4 className="py-3">
                    {props.propsData.description.charAt(0).toUpperCase() +
                        props.propsData.description.slice(1)}
                </h4>
            }



        </div>

    );
}

export default Weather;

function maxminTemp(min, max) {
    if (max && min) {
        return (
            <h3>
                <span className="px-4">{min}&deg;</span>
                <span className="px-4">{max}&deg;</span>
            </h3>
        );
    }
}